<?php
/**
 * Plugin Name: Wordpress eRSO statistics
 * Plugin URI: https://gitlab.com/duh-casa/wp-erso-statistics
 * Description: Plugin for importing the eRSO statistics JSON file
 * Version: 1.0
 * Author: Jure Sah
 * Author URI: http://www.juresah.si
 */

class wp_erso_statistics {

	private $cachePath;
	private $data;

	function __construct() {
		$this->cachePath = dirname(__FILE__)."/cache";
		add_action( 'init', array($this, 'register'));		
	}

	public function register() {
		register_activation_hook( __FILE__, array($this, 'activate'));

		add_shortcode('erso', array($this, 'render'));
	}

	public function activate() {
		mkdir($this->cachePath, 0700);
	}

	public function render($attr = array()) {
		$this->checkCache();

		$data = $this->data;
		foreach(explode(".", $attr['tag']) as $part) {
			if(isset($data[$part])) {
				$data = $data[$part];
			} else {
				$data = 0;
			}
		}
		return $data;
	}

	private function checkCache() {
		$file = $this->cachePath."/statistics.json";
		$updated = False;
		if(!file_exists($file) || time()-filemtime($file) > 2 * 3600) {
			file_put_contents($file, file_get_contents("http://erso/statistics.php"));
			$updated = True;
		}

		if(!isset($this->data) || $updated) {
			$this->data = json_decode(file_get_contents($file), True);
		}
	}

}

new wp_erso_statistics();
